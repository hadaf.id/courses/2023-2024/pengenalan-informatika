# Merepresentasikan Struktur Data Aplikasi dalam Bentuk ER Diagram

## Apa sih arti Entitas dalam dunia Informatika ?
- Dalam dunia Informatika, **Entitas** adalah **representasi** dari **objek** yang terdapat pada berbagai bagian pada **proses bisnis**, **fitur**, **user interface**, maupun **algoritma** dari produk Informatika yang kita bangun
- Kita menentukan Entitas untuk memudahkan perencanaan, perancangan, analisis, hingga pengembangan dan evaluasi produk digital kita
- Penggunaan Entitas dapat berupa:
    - Struktur Table data di Database
    - Perwujudan tiap satuan data pada Table di Database
    - Perwujudan Object pada kode pemrograman
- Contohnya:
    - Keranjang Belanja - Entitas UI pada kode program Front End aplikasi Shopee
    - Pemesanan - Entitas Data di database yang mewakili pemesanan pada aplikasi Shopee
    - Hero - Entitas Data, UI, sekaligus Fitur pada aplikasi Mobile Legend
    - Jendela Video - Entitas UI pada kode program Front End aplikasi Youtube
    - Pemutar Video - Entitas Fitur pada kode program Front End aplikasi Youtube
    - Video - Entitas Data di database yang mewakili data dari tiap video di Youtube
    - Pengelola Video - Entitas Fitur pada kode program Back End di server Youtube

## Bagaimana Penggunaan Entitas Untuk Representasi Struktur Data ?
- Dalam lingkup Database, Entitas mewakili objek-objek satuan data
- Table pada database mewadahi objek-objek data yang sama 

### Contoh Entitas:
- Entitas pada Aplikasi ojek online:
    - Pengguna
    - Pengendara
    - Pemesanan
    - Restoran
    - Menu
    - dsb.
- Entitas pada Game mobile legend:
    - User
    - Hero
    - User Hero
    - Match
    - Match User
    - Match User Hero
    - Match User Hero Equipment
    - dsb.
- Entitas pada Online banking:
    - Pengguna
    - Rekening
    - Transaksi
    - dsb.
- Entitas pada Instagram:
    - Pengguna
    - Pengguna Follower
    - Postingan
    - Postingan Komentar
    - Postingan Penilaian
    - dsb.

## Bagaimana Menentukan Entitas dari Sebuah Sistem ?

Entitas dapat ditentukan berdasarkan :
1. Standard Operating Procedure (SOP) dari organisasi. Contoh:
    - Dalam organisasi perguruan tinggi, terdapat struktur Perguruan Tinggi, Fakultas, Jurusan, Dosen, Mahasiswa, Mata Kuliah dan Kelas
        - Aturannya:
            - Setiap Perguruan Tinggi memiliki >= 1 Fakultas
            - Setiap Fakultas memiliki >= 1 Jurusan
            - Setiap Jurusan memiliki >= 1 Dosen
            - Setiap Jurusan memiliki >= 1 Mata Kuliah
            - dsb.
        - Aturan tersebut maka dapat dibuat beberapa entitas yaitu:
            1. Perguruan Tinggi
            2. Fakultas
            3. Jurusan
            4. Dosen
            5. Mata Kuliah
            6. Kelas
2. User Story ataupun Use Case dari rancangan fitur aplikasi yang ingin dibuat. Contoh:
    - James ingin membuat aplikasi pengingat amanah
        - User Story nya:
            1. Pengguna dapat mendaftarkan dan memberi tenggat deadline pada amanah
            2. Pengguna dapat mengubah status amanah: RENCANA / DIKERJAKAN / SELESAI
            3. Pengguna dapat mengkategorikan amanah menjadi lebih dari satu kategori
            4. Pengguna dapat mengubah profil pengguna
        - Dari User Story di atas, kita dapat melihat bahwa James perlu membuat minimal 3 entitas untuk mewadahi 4 User Story tersebut yaitu:
            1. Amanah
                - ID Amanah
                - ID Pengguna
                - Waktu Mulai
                - Waktu Deadline
                - Status Amanah
            2. Kategori Amanah
                - ID Kategori Amanah
                - Deskripsi
            3. Pengguna
                - ID Pengguna
                - Nama Pengguna
                - Avatar

## Apa itu Atribut Entitas ?
Atribut entitas adalah berbagai atribut yang dimiliki oleh sebuah Entitas.

### Contoh Atribut Entitas:
- Atribut Entitas Mahasiswa:
    - NIM
    - Nama Lengkap
    - Email
    - Tahun Masuk
    - Kode Jurusan
    - dsb.
- Atribut Entitas Hero Mobile Legend:
    - Hero ID
    - Hero Name
    - Hero Type
    - Hero Default Stats
    - dsb.
- Atribut Entitas Postingan:
    - ID Postingan
    - ID Pembuat Postingan
    - Array Alamat Gambar
    - Deskripsi
    - Waktu Pembuatan
    - dsb.

## Apa itu Hubungan Antar Entitas / Entity Relationship (ER) ?
Entity Relationship adalah representasi dari hubungan antar entitas. 

Contoh: 
- Tiap satu Mahasiswa akan registrasi pada beberapa Kelas dalam satu Semester
- Tiap satu Jurusan memiliki beberapa Dosen
- Tiap satu Dosen dapat mengajar di beberapa Kelas
- Tiap satu Player memiliki 1 atau lebih Hero
- Tiap satu Pengguna Instagram dapat memiliki 0 atau lebih Follower
- dsb.

## Bagaimana Cara Merepresentasikan ER dalam Bentuk Diagram ?
ER dapat direpresentasikan dalam diagram menggunakan notasi Entity Relationship Diagram (ER Diagram). Kamu bisa membuat ER Diagram menggunakan pensil dan pulpen di kertas, ataupun dengan menggunakan tools seperti Mermaid JS, Diagrams.net, dbdiagrams.io, dan aneka tool lainnya. ER Diagram sangat umum digunakan untuk merancang struktur database, contohnya ketika Kamu ingin membuat aplikasi ala Gojek dengan database PostgreSQL, ER Diagram akan memudahkanmu membuat struktur Table dalam Database nya.

### Contoh Ojek Online (Bahasa Indonesia)
```mermaid
erDiagram
    PENGGUNA ||--o{ PEMESANAN : pesan
    PENGGUNA {
        string id_pengguna
        string nama_lengkap
        string email
    }
    PEMESANAN {
        string pemesanan
        string id_pengguna
        int tipe_pemesanan
        string id_pengendara
        timestamp waktu_pemesanan
    }
    PENGENDARA ||--o{ PEMESANAN : melayani
    PENGENDARA {
        string id_pengendara
        string nama_lengkap
        string email
    }
```

### Contoh Mobile Legend (Bahasa Inggris)


```mermaid
erDiagram
    PLAYER {
        string player_id
        string player
        string username
        string hashed_password
        string email
        string full_name
    }
    HERO {
        string hero_base_id
        string hero_name
        int hero_role_id
        int hero_speciality_id
    }
    HERO_SKIN {
        string hero_id
        string hero_base_id
        string hero_title
    }
    HERO_SPECIALITY {
        int hero_speciality_id
        string speciality_name
        json speciality_rule
    }
    HERO_SKILL {
        int hero_skill_id
        string skill_name
        json skill_rule
    }
    PLAYER_HERO {
        int player_hero_id
        int hero_id
    }
    PLAYER_HERO_SKIN {
        int player_hero_skin_id
        int player_hero_id        
        datetime obtained_date
    }
    EMBLEM {
        int emblem_id
        int emblem_type_id
        json emblem_rule
    }
    PLAYER_EMBLEM {
        int player_emblem_id
        int player_id
        int emblem_id
        int emblem_score
    }
    MATCH {
        int match_id
        datetime created_time
        int creator_player_id
    }
    MATCH_PARTICIPANT {
        int match_id
        int player_id
        datetime join_time
        int player_hero_skin_id
        int team_number
    }
    PLAYER ||--|{ PLAYER_HERO : has
    PLAYER_HERO ||--|{ PLAYER_HERO_SKIN : has
    HERO_SKIN ||--|{ PLAYER_HERO_SKIN : used_as
    PLAYER ||--|{ PLAYER_EMBLEM : has
    EMBLEM ||--|{ PLAYER_EMBLEM : used_as
    HERO ||--|{ HERO_SKIN : skin
    HERO ||--|{ PLAYER_HERO : used_as
    HERO ||--|| HERO_SPECIALITY : has
    HERO ||--|{ HERO_SKILL : has
    PLAYER ||--O{ MATCH : create
    MATCH ||--|{ MATCH_PARTICIPANT : has
    MATCH_PARTICIPANT ||--O{ PLAYER : joined
    MATCH_PARTICIPANT ||--|| PLAYER_HERO_SKIN : use
```

![player profile](https://pm1.aminoapps.com/7119/2219f9a67e539573c3f4bb03697f3ab564101786r1-1334-750v2_uhq.jpg)

![select player](https://steemitimages.com/p/2gsjgna1uruvUuS7ndh9YqVwYGPLVszbFLwwpAYXYSdbyYb356CunLCEQs64aJAUBh5KugyzbBTH8ALUP3MTBGTFXKum6WE81dU27qMWof7ZmP4YMk?format=match&mode=fit&width=640)

### Contoh Sistem Informasi Akademik Perguruan Tinggi (Bahasa Indonesia)
```mermaid
erDiagram
    KAMPUS ||--|{ FAKULTAS : memiliki
    KAMPUS {
        string id_kampus
        string nama_kampus
        string url_kampus
    }
    FAKULTAS {
        string id_fakultas
        string id_kampus
        string nama_fakultas
        string kode_fakultas
    }
    FAKULTAS ||--|{ JURUSAN : memiliki
    JURUSAN {
        string id_jurusan
        string id_fakultas
        string nama_jurusan
        string kode_jurusan
    }
    JURUSAN ||--|{ MAHASISWA : memiliki
    MAHASISWA {
        string nim_mahasiswa
        string id_jurusan
        string nama_lengkap
        string email
    }
    JURUSAN ||--|{ DOSEN : memiliki
    DOSEN {
        string nip_dosen
        string id_jurusan
        string nama_lengkap
        string email
    }
    JURUSAN ||--|{ MATAKULIAH : memiliki
    MATAKULIAH {
        string kode_matakuliah
        string id_jurusan
        string label_matakuliah
        string deskripsi
    }
    MATAKULIAH ||--|{ KELAS : memiliki
    DOSEN ||--|{ KELAS : keanggotaan
    MAHASISWA }|--|{ KELAS : keanggotaan
    KELAS {
        string id_kelas
        string kode_kelas
        string kode_matakuliah
        string kode_semester
        string nip_dosen
    }
```

