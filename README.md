# Pengenalan Informatika

## Bahasan
1. Pengantar Informatika, filosofi, dan teknologi pendukung 
2. Case produk teknologi informasi beserta aspek keinformatikaan di dalamnya 
3. Cara kerja mesin komputasi 
4. Sistem operasi dan program komputer 
5. Algoritma, struktur data, dan bahasa pemrograman 
6. Pengembangan perangkat lunak 
7. Manajemen informasi
    - [Merepresentasikan struktur data dalam bentuk ER Diagram](./er-diagram)
8. Job interview
9. Internet, server-client, dan cloud service
10. Pemrosesan multimedia
11. Artificial intelligence dan data science
12. Cybersecurity dan cryptography
13. Distributed computing, ubiquitous computing, dan big data
14. Penelitian dan publikasi ilmiah
15. Profesi keinformatikaan 
16. Startup pitching 
